# marpe

A web-based health record and hospital information system (HIS) developed using a microservices based architecture and managed using containers.

# Why Containers?
Enterprises are increasingly experiencing cost savings, solving deployment problems, and improving DevOps and production operations by using containers

# Why Microservices?
The term "Microservice Architecture" has sprung up over a few years now. It describes a particular way of designing software applications as suites of independently deployable services. 

It has emerged as an important approach for distributed mission-critical applications. Each service can be developed, tested, deployed and versioned independently.

Each service will run in its own process and communicate with other processes using protocols such as HTTP/HTTPS, WebSockets, or Advanced Messaging Queuing Protocol 